client.select('*').from(opts.table).stream()
  .pipe(through.obj(function chunkToFiles (chunk, enc, callback) {
    const primaryKey = chunk[opts.primaryKey]
  
    Object.keys(chunk).forEach((key) => {
      this.push(new File({
        path: `./${primaryKey}/${key}`,
        contents: new Buffer(chunk[key] + '')
      }))
    })
    
    callback()
  }))
  .pipe(vfs.dest('./' + opts.table))

